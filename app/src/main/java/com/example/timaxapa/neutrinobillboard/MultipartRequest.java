package com.example.timaxapa.neutrinobillboard;

import com.android.volley.*;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for executing multipart requests
 */
public class MultipartRequest extends Request<String> {

    private HttpEntity mHttpEntity;
    private Response.Listener mListener;

    public MultipartRequest(String url,
                            Response.Listener<String> mListener,
                            Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);

        this.mListener = mListener;
        mHttpEntity = buildMultipartEntity();
    }

    private HttpEntity buildMultipartEntity() {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        // Setting string params
        try {
            for (String key : getParams().keySet()) {
                builder.addPart(key, new StringBody(getParams().get(key),
                        ContentType.TEXT_PLAIN));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Setting files
        for (String key : getFiles().keySet()) {
            FileBody fileBody = new FileBody(getFiles().get(key));
            builder.addPart(key, fileBody);
        }

        return builder.build();
    }

    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    // Adding files to request
    public Map<String, File> getFiles() {
        return new HashMap<>();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        return Response.success(new String(response.data), getCacheEntry());
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }
}