package com.example.timaxapa.neutrinobillboard.domain;

import android.graphics.Bitmap;
import com.google.gson.annotations.Expose;

import java.util.Date;

public class Advertisement {

    @Expose(deserialize = true)
    private String id;
    @Expose(deserialize = true)
    private String name;
    @Expose(deserialize = true)
    private String description;
    @Expose(deserialize = true)
    private Date createdAt;
    private Bitmap image;

    public Advertisement() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

}
