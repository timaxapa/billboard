package com.example.timaxapa.neutrinobillboard;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper {

    public static final String FILE_NAME = "settings";
    public static final String USER_ID = "user_id";

    private Context mContext;
    private SharedPreferences mSharedPreferences;

    public PreferencesHelper(Context mContext) {
        this.mContext = mContext;
        mSharedPreferences = mContext.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public String getUserId() {
        String userId = mSharedPreferences.getString(USER_ID, null);
        return userId;
    }

    public void setUserId(String userId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_ID, userId);
        editor.commit();
    }
}
